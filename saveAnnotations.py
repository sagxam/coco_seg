import os
import json
import joblib
from tqdm import tqdm


path1 = "../data/coco/annotations/instances_train2017.json"
path2 = "../data/coco/annotations/instances_val2017.json"


train_bin_path = "./picklefiles/train_annotations.bin"
val_bin_path = "./picklefiles/val_annotations.bin"





def saveAnnotations(data_path_list,dump_path_list):
	
	print("*"*30)
	print("Saving annotations in binary format")

	for i,j in zip(data_path_list,dump_path_list):

		if not os.path.exists(j):
			with open(i, "r") as json_file:
				annotations = json.load(json_file)

				joblib.dump(annotations, j, compress=9)
				print("Dumped ",j)
				print("as object type: ", type(annotations))


	print("Saving annotations completed")
	print("*"*30)




data_path_list = [path1,path2]
dump_path_list = [train_bin_path, val_bin_path]
saveAnnotations(data_path_list,dump_path_list)