import numpy as np
import pandas as pd
import skimage.io as io
import matplotlib.pyplot as plt


class Plots:
    """
    Class for plots
    """

    def __init__(self):
        pass

    ##############################################################################
    ###### Specific Function Visualise image mask and image ######################
    ##############################################################################

    def plot_img_and_mask(self, img_id, reqd_mask, img_path_df):
        """
        Visualise image and it's mask
        args:
                        img_id: id of image
                        mask_value: reqd mask of image (numpy array)
                        img_path_df: dataframe containing (id, path_to_image)
        returns:
                        matplotlib plot of img and mask
        """

        req_path = img_path_df[img_path_df['id'] == img_id]['path'].values[0]
        I = io.imread(req_path)

        f, ax = plt.subplots(1, 2)

        ax[0].imshow(I)
        try:
            ax[1].imshow(reqd_mask)
        except Exception as e:
            print("loadData.py method-> plot_img_and_mask:\nError", e)
            # print("No mask present for required category check category id.")

        plt.plot()





    ##############################################################################
    ###### Specific Function Visualise confusion matrix of mask ##################
    ##############################################################################

    def get_elements_confusion_matrix(self, mask_true, mask_pred):
        """
        Utility function to get elements of confusion matrix

        Args:
                mask_true (numpy array): True mask
                mask_pred (numpy array): Predicted mask

        Returns:
                tuple: (TP, FN, TN, FP)
        """
        TP = np.bitwise_and(mask_true, mask_pred)
        TP[TP == 255] = 0

        FN = np.bitwise_not(mask_true) - np.bitwise_not(mask_pred)
        FN[FN == 255] = 0

        TN = np.bitwise_not(np.bitwise_and(mask_true, mask_pred))
        TN[TN == 255] = 0
        TN = np.bitwise_not(TN)

        FP = mask_true - mask_pred
        FP[FP == 255] = 0

        return (TP, FN, TN, FP)

    

    def plot_confusion_matrix(self, mask_true, mask_pred, img=None):
        """
        Plot Confusion matrix for image segmentation

        Args:
                mask_true (numpy array): True mask
                mask_pred (numpy array): Predicted mask
                img (numpy array): required image
        """
        TP, FN, TN, FP = self.get_elements_confusion_matrix(mask_true, mask_pred)

        if type(img) == type("NoneType"):
            plt.subplot(2, 2, 1)
            plt.title("TP")
            plt.imshow(TP, cmap="gray")

            plt.subplot(2, 2, 2)
            plt.title("FN")
            plt.imshow(FN, cmap="gray")

            plt.subplot(2, 2, 3)
            plt.title("FP")
            plt.imshow(FP, cmap="gray")

            plt.subplot(2, 2, 4)
            plt.title("TN")
            plt.imshow(TN, cmap="gray")

        else:
            plt.subplot(3, 2, 1)
            plt.title("mask_true")
            plt.imshow(mask_true)

            plt.subplot(3, 2, 2)
            plt.title("mask_pred")
            plt.imshow(mask_pred)

            plt.subplot(3, 2, 3)
            plt.title("TP")
            plt.imshow(TP, cmap="gray")

            plt.subplot(3, 2, 4)
            plt.title("FN")
            plt.imshow(FN, cmap="gray")

            plt.subplot(3, 2, 5)
            plt.title("FP")
            plt.imshow(FP, cmap="gray")

            plt.subplot(3, 2, 6)
            plt.title("TN")
            plt.imshow(TN, cmap="gray")

        plt.plot()
        plt.show()
