#!/usr/bin/env python
# coding: utf-8

# # Initial Block

# In[1]:


from nntool import unet
from utils.utilities import devtools
import os
import glob
import time
import joblib

import math
import tensorflow as tf
import pandas as pd
import numpy as np
import json
from datetime import datetime


import matplotlib.pyplot as plt

from skimage import io
from skimage.transform import rescale, resize, downscale_local_mean, rotate
from glob import glob
from datetime import datetime
import subprocess
from tqdm import tqdm

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, MaxPool2D, MaxPooling2D, UpSampling2D, Conv2DTranspose
from tensorflow.keras.layers import Dropout, BatchNormalization
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import concatenate
from tensorflow.keras import backend as K
from tensorflow.keras import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import *
from tensorflow.python.client import device_lib
from tensorflow.keras.models import load_model

from tensorflow.keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img


from pprint import pprint
print("#"*40)
print("#"*40)

print("\nTensorflow version ", tf.__version__,"\n")

print("#"*40)
print("#"*40)


dtools = devtools()
"""
https://www.youtube.com/watch?v=Gl-N3xr5zLI
Add min_delta, period

tf.train.latest_checkpoint

try increasing bottleneck 
it has more generality
"""

input_params = {
    
    # model training parameters 

    "depth" : 5,
    "batch_size" : 8,
    "epochs" : 100,
    "SEED" : np.random.randint(1000,7000),
    "IMG_HEIGHT" : 256,
    "IMG_WIDTH" : 256,
    "input_channels" : 1,
    "steps_per_epoch" : 1000, # (64115 // batch_size) 
    "validation_steps" : 250, # (2693 // batch_size)
    "learning_rate" : 1e-3,
    "use_multiprocessing" : True,
    "shuffle" : True,
    "patience":10,

    # verbose parameters
    "disp_gpu" : 'n',
    "TF_CPP_MIN_LOG_LEVEL":'2',

    # data generator parameters
    "rotation_range":45,
    "horizontal_flip":True,
    "vertical_flip":False,
    "fill_mode": "nearest",
    "width_shift_range" : 0.2,
    "height_shift_range" : 0.2,
    "shear_range" : 0.2,
    "zoom_range" : 0.2,
    "rescale" :1./255,

    # load model weights ?
    "load_model" : 'y',
}

os.environ['TF_CPP_MIN_LOG_LEVEL']=input_params['TF_CPP_MIN_LOG_LEVEL']


# In[2]:

# _ = input("Display device and gpu details [y/n] ")
_ = input_params['disp_gpu']
if _ == 'y':
        print(device_lib.list_local_devices())
else:
        pass


# # Directory Structure

# In[3]:

print()
os.system("tree ../data/coco-person/ --dirsfirst --filelimit 10")
print()
print("#"*40)

# # Get Data ids

# In[4]:


def get_data_ids():
        train_imgs = []
        train_masks = []

        val_imgs = []
        val_masks = []

        common_path = "../data/coco-person/"
        paths = [
                "train/person_images/images/*", "train/person_masks/masks/*",
                "val/person_images/images/*", "val/person_masks/masks/*"]
        # final list var will have => [train_imgs, train_masks, val_imgs, val_masks]

        var = []

        for i in paths:
                # print(os.path.join(common_path,i))
                var.append(glob(os.path.join(common_path, i)))

        return var


train_imgs_dir = "../data/coco-person/train/person_images/"
train_masks_dir = "../data/coco-person/train/person_masks/"

val_imgs_dir = "../data/coco-person/val/person_images/"
val_masks_dir = "../data/coco-person/val/person_masks/"


X_train, y_train, X_test, y_test = get_data_ids()


# In[5]:


# print("total training person images {" ":>10}".format(len(X_train)))
# print("total training person masks {" ":>11}".format(len(y_train)))

# print("total validation person images {" ":>8}".format(len(X_test)))
# print("total validation person masks {" ":>9}".format(len(y_test)))


# # Parameters

# In[6]:
print("#"*40)
print("\nSetting Parameters ...\n")
print("#"*40)

depth = input_params['depth']
batch_size = input_params['batch_size']
epochs = input_params['epochs']


SEED = input_params['SEED']
IMG_HEIGHT = input_params['IMG_HEIGHT']
IMG_WIDTH = input_params['IMG_WIDTH']
input_image_channels = input_params['input_channels']

if input_image_channels == 3:
        color_mode = 'rgb'
if input_image_channels == 1:
        color_mode = 'grayscale'

# this will do batch training
steps_per_epoch_full = (len(X_train) // batch_size)
validation_steps_full = (len(X_test) // batch_size)

print("#"*40)
print("\nTraining Steps per epoch to cover all dataset are {}".format(
        steps_per_epoch_full))
print("Validation Steps per epoch to cover all dataset are {}".format(
        validation_steps_full),"\n")


# as we are augmenting dataset
# and our dataset is very huge
# we need mini batches

steps_per_epoch = input_params['steps_per_epoch']
validation_steps = input_params['validation_steps']


print("\nSetting steps_per_epoch to {}".format(steps_per_epoch))
print("Setting validation_steps to {}".format(validation_steps),"\n")

print("_"*40,"\n")
# In[ ]:


# # Image Data Generator

# In[7]:


image_datagen = ImageDataGenerator(
        rotation_range=input_params['rotation_range'],
        horizontal_flip=input_params['horizontal_flip'],
        vertical_flip=input_params['vertical_flip'],
        fill_mode=input_params['fill_mode'],
        width_shift_range=input_params['width_shift_range'],
        height_shift_range=input_params['height_shift_range'],
        shear_range=input_params['shear_range'],
        zoom_range=input_params['zoom_range'],
        rescale=input_params['rescale']
)


mask_datagen = ImageDataGenerator(
        rotation_range=input_params['rotation_range'],
        horizontal_flip=input_params['horizontal_flip'],
        vertical_flip=input_params['vertical_flip'],
        fill_mode=input_params['fill_mode'],
        width_shift_range=input_params['width_shift_range'],
        height_shift_range=input_params['height_shift_range'],
        shear_range=input_params['shear_range'],
        zoom_range=input_params['zoom_range'],
        rescale=input_params['rescale']
)


# # Flow from directory

# ## train generators

# In[8]:


train_img_generator = image_datagen.flow_from_directory(batch_size=batch_size,
                                                        directory=train_imgs_dir,
                                                        shuffle=True,
                                                        target_size=(
                                                            IMG_HEIGHT, IMG_WIDTH),
                                                        class_mode=None,
                                                        color_mode=color_mode,
                                                        seed=SEED)


train_mask_generator = image_datagen.flow_from_directory(batch_size=batch_size,
                                                         directory=train_masks_dir,
                                                         shuffle=True,
                                                         target_size=(
                                                                 IMG_HEIGHT, IMG_WIDTH),
                                                         class_mode=None,
                                                         color_mode='grayscale',
                                                         seed=SEED)


# ## validation generators

# In[9]:


val_img_generator = image_datagen.flow_from_directory(batch_size=batch_size,
                                                    directory=val_imgs_dir,
                                                    shuffle=True,
                                                    target_size=(
                                                            IMG_HEIGHT, IMG_WIDTH),
                                                    class_mode=None,
                                                    color_mode=color_mode,
                                                    seed=SEED)


val_mask_generator = image_datagen.flow_from_directory(batch_size=batch_size,
                                                     directory=val_masks_dir,
                                                     shuffle=True,
                                                     target_size=(
                                                             IMG_HEIGHT, IMG_WIDTH),
                                                     class_mode=None,
                                                     color_mode='grayscale',
                                                                                                         seed=SEED)

val_generator = zip(val_img_generator, val_mask_generator)


# In[10]:


def image_mask_generator(image_data_generator, mask_data_generator):
        train_generator = zip(image_data_generator, mask_data_generator)
        for (img, mask) in train_generator:
                yield (img, mask)
print()
print("_"*40)
print()

print("#"*40)
print("#"*40)
print("Data Generators Set ...")
print("#"*40)


# # Visualise generator images

# In[11]:


# sample_images = next(train_img_generator)
# sample_masks = next(train_mask_generator)


# In[12]:


def plot_sample_batch_images(images_arr):
        fig, axes = plt.subplots(1, 5, figsize=(20, 20))
        axes = axes.flatten()

        for img, ax in zip(images_arr, axes):
                ax.imshow(array_to_img(img))
                ax.axis('off')
        plt.tight_layout()
        plt.show()


# In[13]:


# plot_sample_batch_images(sample_images[:5])


# In[14]:


# plot_sample_batch_images(sample_masks[:5])


# # Create Model

# In[15]:

print("#"*40)
print("\nCreated UNet ...\n")



model = unet.custom_unet(depth=depth, input_shape=(
        IMG_HEIGHT, IMG_WIDTH, input_image_channels), verbose=False)
lr = input_params['learning_rate']

# print("learning_rate = ", lr, "depth = ", input_params['depth'] ,"\n")

for i in (input_params.keys()):
    if i == "rotation_range":
        break
    
    print("{0:<20s} : ".format(i),"{0:<10}".format(str(input_params[i])))

print()
print("#"*40)
print()
# ans = input("Do you want to load trained model [y/n] ")
ans = input_params['load_model']

if ans == 'y':
    print("Choose weights ...\n")
    l = list(sorted(glob("./logs/models/*")))
    pprint(list(enumerate(sorted(glob("./logs/models/*")))))

    _ = int(input("Enter folder number -> "))

    p = l[_]
    l = sorted(glob(p+"/*"))
    pprint(list(enumerate(sorted(glob(p+"/*")))))
    _ = int(input("Enter file number -> "))
    req_p = l[_]
    print(req_p)

    model = load_model(req_p, compile=False)
    model.compile(optimizer=Adam(lr=lr), loss=unet.dice_coef_loss,
                                metrics=[unet.dice, unet.iou])

else:
    model.compile(optimizer=Adam(lr=lr), loss=unet.dice_coef_loss,
                                    metrics=[unet.dice, unet.iou])


# In[16]:


def exp_decay(epoch):
    return 0.001 * math.exp(0.01 * (5 - epoch))


logdir = "./logs/fit/" + datetime.now().strftime("%y%m%d-%H%M%S")
ckptdir = "./logs/models/"+datetime.now().strftime("%y%m%d-%H%M%S")+"_" + \
        str(depth)+"U_ep{epoch:02d}-t-{dice:.3f}-v-{val_dice:.3f}.h5"

callbacks = [
        ReduceLROnPlateau(factor=0.1, patience=5, min_lr=0.000001, verbose=1),
        ModelCheckpoint(ckptdir, monitor='val_dice',
                        verbose=1, save_best_only=True, mode='max',
                        ),  # mode is max
        EarlyStopping(monitor='val_dice', mode='max', patience=input_params['patience'], verbose=1),
        TensorBoard(log_dir=logdir),
        # LearningRateScheduler(exp_decay, verbose=1)

]


# In[17]:


train_generator = image_mask_generator(
        train_img_generator, train_mask_generator)
val_generator = image_mask_generator(val_img_generator, val_mask_generator)


# In[18]:
print("#"*40)
print("Begin Training UNet ...")
print("#"*40)
"""
_ = input("Do you want to resume training [y/n] ")

if _ == 'y':
    initial_epoch = int(input("At which epoch do you want to resume training "))
else:
    initial_epoch = None # <- error here
"""


start = time.time()

history = model.fit_generator(
        train_generator,
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        validation_data=val_generator,
        validation_steps=validation_steps,
        verbose=1,
        callbacks=callbacks,
        use_multiprocessing=input_params['use_multiprocessing'],
        shuffle=input_params['shuffle'],
        # initial_epoch = initial_epoch
)

end = time.time()








# RECORDING AND TIME

import json
data = input_params
path_ = logdir+"/input_params.json"
with open(path_, 'w') as fp:
    json.dump(data, fp)


def timer(start,end):
    hours, rem = divmod(end-start, 3600)
    minutes, seconds = divmod(rem, 60)
    print("\nTime elapsed: ","{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))



timer(start,end)