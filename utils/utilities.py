import os
from glob import glob
import time
import joblib

import pandas as pd
import numpy as np
import json
from datetime import datetime





class devtools:
	"""docstring for devtools"""
	def __init__(self):
		pass

			

	########################################
	######### READ JSON FILE ###############
	########################################

	def read_json(self, path):
		"""
		Read json file
		
		args: 
			path: path to required file
		returns:
			dict
		"""
		with open(path,"r") as json_file:
			d = json.load(json_file)
			
			print(d.keys())
			return d
		
		
	########################################
	######### GET TRAIN VAL DF #############
	########################################
	def get_reqd_train_val_df(self, img_path_df, category_name, cat_id=None):
		
		pattern1 = "./picklefiles/train_dataframes/train_"+category_name+"_df.*"  
		pattern2 = "./picklefiles/val_dataframes/val_"+category_name+"_df.*"  
		
		for i in glob(pattern1):
			p1 = i
		
		for i in glob(pattern2):
			p2 = i
		
		print(p1)
		print(p2)
		
		train_df = joblib.load(p1)
		val_df = joblib.load(p2)
		
		ctrain_df = pd.merge(train_df,img_path_df,on="id", how="left")
		ctrain_df = ctrain_df.drop(columns=['license','date_captured'])

		cval_df = pd.merge(val_df, img_path_df, on="id", how="left", )
		cval_df = cval_df.drop(columns=['license','date_captured'])
		return (ctrain_df, cval_df)

	########################################
	######### PROC IMG MASK ################
	########################################
	def resize_image(self, a,newshape, is_mask=False):
		if is_mask:
			newshape_mask = (newshape[0],newshape[1],1)
			return resize(a,output_shape=newshape_mask)
		else:
			return resize(a,output_shape=newshape)

	def to_float(a):
		return np.float32(a)


	def preprocess_nn(self, a,newshape, is_mask=False):
		a = to_float(a)
		a = resize_image(a,newshape,is_mask)
		
		return a

	def reshape_fit(self, a):
		a = np.array([a])
		return a

	########################################
	######### LOAD IMG MASK ################
	########################################

	def load_image_mask_online(self, coco, co_ob, img_id, cat_id, img_path_df, master_df, newshape=(256,256,3)):
		t = img_path_df[img_path_df['id'] == img_id]
		
		req_url = t['coco_url'].values[0]
		# print(req_url)
		
		
		img = io.imread(req_url)
		mask = co_ob.get_mask_of_image(coco, img_id, cat_id=cat_id, img_path_df=img_path_df, master_df=master_df)
		
		img = preprocess_nn(img, newshape)
		mask = preprocess_nn(mask, newshape, is_mask=True)
		
		return (img,mask)

		
	########################################
	######### PLOT IMG MASK ################
	########################################
		
	def plot_img_and_mask(self, img,mask):
		"""
		Visualise image and it's mask
		args:
			img: image (numpy array)
			mask: reqd mask of image (numpy array)
		returns:
			matplotlib plot of img and mask
		"""

		I = img

		f, ax = plt.subplots(1, 2)
		ax[0].imshow((I).astype(np.int32))
		ax[1].imshow(np.uint8(np.squeeze(mask)*255))
		
		plt.plot()
		plt.show()

		
	########################################
	######### TIMER METHOD  ################
	########################################
		
	def timer(self, start,end):
		hours, rem = divmod(end-start, 3600)
		minutes, seconds = divmod(rem, 60)
		print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
		
		
		

	########################################
	###### HELPER STEP TRAIN METHOD  #######
	########################################
		
	def load_params_prev_run(self, params, step_size=None, set_next=False, verbose=False):
		
		lgs = glob(pathname="/content/drive/My Drive/coco_segment/coco_seg/logs/runs/*.lg")
		lgs.sort(key=os.path.getmtime)
		
		try:
			print("Loading past run info: ",lgs[-1])
			lg_keeper_dict = joblib.load(lgs[-1])
		except Exception as e:
			print("This might be 1st run")
			print("No file found Path erorr")
			return params
		
		
		for i,j in enumerate(lg_keeper_dict.keys()):
			if verbose:
				print()
				print(j," ",params[i],"<-",log_keeper_dict[j])
			params[i]=lg_keeper_dict[j]
		
		print()
		
		if set_next:
			"""
			This needs to be done
			start_ix_train = end_ix_train
			end_ix_train += step_size
			
			start_ix_train is params[2]
			end_ix_train is param[3]
			
			similarly for start and end val_ix
			param[4], param[5] resp
			"""
			
			
			params[2] = params[3]
			params[3] += step_size
			
			
			# this line will create errors 
			# when val dataset is smaller than 
			# train dataset
			# val set will exhaust
			params[4] = params[5]
			params[5] += step_size
		return params
