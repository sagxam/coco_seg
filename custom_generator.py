import numpy as np
import pandas as pd
import joblib
from skimage import io, transform
from skimage.transform import resize, rotate
import matplotlib.pyplot as plt
import os
from tqdm import tqdm
from glob import glob

from skimage import exposure
from skimage import img_as_float
from scipy import ndimage
from skimage.util import random_noise
import random
from pprint import pprint
import sys


from tensorflow.keras.utils import Sequence
import warnings

warnings.filterwarnings('ignore')


from dataflow import ImageOperations


class KGenerator():
    """
    Class for generating images
    """

    def __init__(self, im_df, failed_df, train_range=None,
                IMG_HEIGHT=256, IMG_WIDTH=256, as_gray=True,
                contrast_adjust=True, hist_equal=True,
                horizontal_flip=True, vertical_flip=True,
                random_rotation=True, random_zoom=True,
                gamma_correction=True, blur=True, add_noise=True,
                show_NOT_person_imgs=True,
                fit_op=True):
    
        """Summary
        
        Args:
            reqd_full_df (pd.DataFrame): path_to_img|path_to_mask|thold
        """


        im_df['thold'] = 'Y'
        failed_df['thold'] = 'N'
        
        reqd_full_df = im_df
        # reqd_full_df = pd.concat([im_df,failed_df],axis=0,ignore_index=True)
        self.reqd_full_df = reqd_full_df
        self.reqd_full_df = self.reqd_full_df.sample(frac=1).reset_index(drop=True)

        try:
            self.reqd_full_df = self.reqd_full_df[train_range[0], train_range[1]]
        except Exception as e:
            # print("Enter valid train_range")
            pass

        if 'train' in self.reqd_full_df.loc[0].values[0]:
            self.type_of_dataset = 'Train set'
        else:
            self.type_of_dataset = 'Validation set'


        self.IMG_HEIGHT, self.IMG_WIDTH = IMG_HEIGHT, IMG_WIDTH
        
        self.train_range = train_range

        self.as_gray = as_gray
        

        self.img = None
        self.msk = None
        
        self.no_person_img = None
        self.no_person_msk = None
                

        self.horizontal_flip = horizontal_flip
        self.vertical_flip = vertical_flip
        self.random_zoom = random_zoom
        self.random_rotation = random_rotation
        self.contrast_adjust = contrast_adjust
        self.hist_equal = hist_equal

        self.gamma_correction = gamma_correction
        self.blur = blur
        self.add_noise = add_noise
        
        
        self.fit_op = fit_op
        self.show_NOT_person_imgs = show_NOT_person_imgs
        
        


        self.iob = ImageOperations(self.IMG_HEIGHT, self.IMG_WIDTH, self.as_gray, self.fit_op)
        self.iops_dict = {
            "contrast_adjust": self.iob.contrast_adjust,
            "horizontal_flip": self.iob.horizontal_flip_op,
            "vertical_flip": self.iob.vertical_flip_op,
            "random_rotation": self.iob.rotation_op,
            "random_zoom": self.iob.zoom_op,
            "gamma_correction": self.iob.gamma_op,
            "blur":self.iob.blur_op,
            "add_noise":self.iob.gaussian_noise,
            "show_NOT_person_imgs": self.iob.show_not_person,
        }

        self.COUNT = 0
        

    def augmentations(self, img_path, msk_path, thold, **kwargs):
        """

        img, msk: image and respective mask
        thold(bool): threshold for training 

        `kwargs`:

        
        contrast_adjust: True/False,
        horizontal_flip: True/False,
        vertical_flip: True/False,
        random_rotation: True/False,
        random_zoom: True/False,
        gamma_correction: True/False,
        blur:True/False,
        add_noise:True/False,
        show_NOT_person_imgs: True/False,


        """
        # print(kwargs.keys())
        # print(kwargs.values())

        params = kwargs

        if thold == 'Y':
            del params['show_NOT_person_imgs']

        for k in list(params.keys()):
            if params[k] == False:
                del params[k]



        ix = np.random.randint(0,len(params))
        # print(ix)

        keys = list(params.keys())
        vals = list(params.values())
        
        # print(keys[ix], vals[ix])
        # get required function from available operations
        f = self.iops_dict[keys[ix]]

        # print(f)

        c_img, c_msk = self.iob.pass1(img_path, msk_path, self.IMG_HEIGHT, self.IMG_WIDTH, self.as_gray)
        c_img, c_msk = self.iob.histogram_equalization(c_img,c_msk)
                
        
        i,m = f(c_img, c_msk)

        return i,m


    def kgenerator(self):
        while True:
            for V in list(self.reqd_full_df.values):
                # print("Batch {}".format(i))
                
                # print(V[2])
                # if image has crossed threshold
                
                i,m = self.augmentations(
                    V[0],V[1],V[2],
                    contrast_adjust=self.contrast_adjust,
                    horizontal_flip=self.horizontal_flip,
                    vertical_flip=self.vertical_flip,
                    random_rotation=self.random_rotation,
                    random_zoom=self.random_zoom,
                    blur=self.blur,
                    show_NOT_person_imgs=self.show_NOT_person_imgs,
                    )

                yield i,m
            print("\nEnd of {} shuffled and repeating...\n".format(self.type_of_dataset))
            self.reqd_full_df = self.reqd_full_df.sample(frac=1).reset_index(drop=True)


class KDataFlow():

    """
    Class for passing batches of images to fit operation

    """

    def __init__(self, KGenerator_object, shuffle_after_epoch=False, batch_size=2):

        
        self.batch_size = batch_size
        
        self.shuffle_after_epoch = shuffle_after_epoch
        
        
        
        self.kob = KGenerator_object
        self.generator = self.kob.kgenerator()


        self.full_len = self.kob.reqd_full_df.shape[0]


        


    def __len__(self):
        return (self.full_len//self.batch_size)


    def getitem(self):
        while True:
            imgs, masks = [], []
            for k in range(self.batch_size):
                try:
                    i,m = next(self.generator)
                except StopIteration:
                    return
                imgs.append(i)
                masks.append(m)
            
            imgs = np.array(imgs).astype(np.float32)
            masks = np.array(masks).astype(np.float32)
            
            try:
                yield imgs, masks
            except StopIteration:
                return

    def on_epoch_end(self):
        if self.shuffle_after_epoch:
            self.reqd_full_df = self.kob.reqd_full_df.sample(frac=1).reset_index(drop=True)

