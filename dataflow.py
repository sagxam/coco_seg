import numpy as np
import pandas as pd
import joblib
from skimage import io, transform
from skimage.transform import resize, rotate
import matplotlib.pyplot as plt
import os
from tqdm import tqdm
from glob import glob

from skimage import exposure
from skimage import img_as_float
from scipy import ndimage
from skimage.util import random_noise
import random
from pprint import pprint


import warnings
warnings.filterwarnings('ignore')




class ImageOperations:

    """
    Variety of Image operations for augmentation
    
    Attributes:
        as_gray (bool): load grayscale image
        fit_op (bool): reshape (1,X,Y,1)
        img (numpy array): Image
        msk (numpy array): Mask
    """
    
    def __init__(self,IMG_HEIGHT=256, IMG_WIDTH=256, as_gray=True, fit_op=True):
        self.img = None
        self.msk = None
        self.fit_op = fit_op
        self.IMG_HEIGHT, self.IMG_WIDTH = IMG_HEIGHT, IMG_WIDTH
        self.as_gray =as_gray

    ###################################
    ####### START of OPERATIONS #######
    ###################################

    def pass1(self, img_path, mask_path, IMG_HEIGHT, IMG_WIDTH, as_gray=True):
        self.img = io.imread(img_path, as_gray=as_gray)
        self.msk = io.imread(mask_path, as_gray=as_gray)

        img = resize(self.img, (self.IMG_HEIGHT, self.IMG_WIDTH))
        msk = resize(self.msk, (self.IMG_HEIGHT, self.IMG_WIDTH))

        
        
        msk = np.where(msk>0.5,1,0)
        
        self.img = img
        self.msk = msk

        IMG,MSK =  img, msk
        # print(IMG.shape, MSK.shape)
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)
    
    def histogram_equalization(self, img, msk):
        img = exposure.equalize_hist(img)
        

        # img = exposure.equalize_adapthist(img, clip_limit=0.03) <<<< does NOT work
        # contrast limited adaptive hist equalization
        # clahe = cv2.createCLAHE(clipLimit=2, tileGridSize=(8,8))
        # img = clahe.apply(img)
        
        
        
        IMG,MSK =  img, msk
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)
    
    
    def contrast_adjust(self, img, msk):
        p2, p98 = np.percentile(img, (2, 98))
        img = exposure.rescale_intensity(img, in_range=(p2, p98))
        
        IMG,MSK =  img, msk
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)
    
    

    def horizontal_flip_op(self, img, msk):
        
        IMG,MSK =  np.fliplr(img), np.fliplr(msk)
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)

    def vertical_flip_op(self, img, msk):
        
        IMG,MSK =  np.flipud(img), np.flipud(msk)
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)

    def rotation_op(self, img, msk, rotation_range=45):
        r = np.random.randint(low=-rotation_range, high=rotation_range)

        
        img, msk =  rotate(img, r), rotate(msk, r)

        msk = (msk - np.min(msk.flatten()))/(np.max(msk.flatten()) - np.min(msk.flatten()))
        msk = np.where(np.squeeze(msk)>0.5,1,0)

        IMG, MSK = img, msk
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)
    
    
    def center_crop_op(self, img, cropx, cropy):
        # print(img.shape)
        y,x = img.shape
        startx = x//2-(cropx//2)
        starty = y//2-(cropy//2)    
        return img[starty:starty+cropy,startx:startx+cropx]
    
    def zoom_op(self, img, msk, zoom_range=3):
        r = random.uniform(1,zoom_range)
        
        img, msk = np.squeeze(img), np.squeeze(msk)
        
        img = ndimage.zoom(img,r)
        msk = ndimage.zoom(msk,r)
        
        img = self.center_crop_op(img, self.IMG_HEIGHT, self.IMG_WIDTH)
        msk = self.center_crop_op(msk, self.IMG_HEIGHT, self.IMG_WIDTH)
        img, msk = resize(img, (self.IMG_HEIGHT, self.IMG_WIDTH)), resize(msk, (self.IMG_HEIGHT, self.IMG_WIDTH))
        
        
        # do min max scaling of mask and image
        # as values of pixels goes above 1 and below 0 sometimes
        
        img = (img - np.min(img.flatten()))/(np.max(img.flatten()) - np.min(img.flatten()))
        msk = (msk - np.min(msk.flatten()))/(np.max(msk.flatten()) - np.min(msk.flatten()))
        
        
        IMG, MSK = img, msk
        
        
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)
    

        
    def gamma_op(self, img, msk):
        adjusted_gamma_img = exposure.adjust_gamma(img, gamma=0.4, gain=0.9)
        
        IMG,MSK = adjusted_gamma_img, msk
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)

    def blur_op(self, img, msk):
        # print(img.shape)
        blured_img = ndimage.uniform_filter(img, size=(3, 3, 1))
        blured_msk = msk  # don't blur the mask
        
        IMG,MSK =  (blured_img, blured_msk)
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)

    def gaussian_noise(self, img, msk):
        
        IMG,MSK =  random_noise(img), msk
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)



    def show_not_person(self, img, msk):
        """
        This method assumes all pixel values of image are between 0 and 1

        """


        ix = np.where(msk==1.)

        """ # this will put random noise in person's place
        for i,j,k in zip(ix[0], ix[1], ix[2]):
            im[i,j,k] = np.random.random_sample()
        """

        # set all pixels of person to 0
        img[ix] = 0


        # set mask area of person to 0
        msk = np.ones(shape=(msk.shape[0],msk.shape[1],1))*0

        IMG , MSK = img, msk
        if not self.fit_op:
            return IMG, MSK
        else:
            return IMG.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1), MSK.reshape(self.IMG_HEIGHT, self.IMG_WIDTH, 1)


    ###################################
    ######### END of OPERATIONS #######
    ###################################