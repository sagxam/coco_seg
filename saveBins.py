import numpy as np
import pandas as pd
import joblib
import time
import os
import json
from skimage import io
from tqdm import tqdm
from glob import glob

import matplotlib.pyplot as plt

# custom modules
from loadData import LoadData, CocoHelper, SaveObjects, LoadBin


print("*****************************************************************************\n")
print("********************** BINARY FILES GENERATOR CODE **************************\n")
print("*****************************************************************************\n")

start_time = time.time()


def check_path_exists(path):
	if os.path.exists(path):
		return True
	else:
		return False


def set_up_picklefiles_dir():
	print("Checking directory structure ... ")

	if not check_path_exists("./picklefiles"):
		os.mkdir("./picklefiles")

	paths = [
		"./picklefiles/train_dataframes",

		"./picklefiles/1CH_train_masks",
		"./picklefiles/3CH_train_masks",
		
		"./picklefiles/val_dataframes",

		"./picklefiles/1CH_val_masks",
		"./picklefiles/3CH_val_masks"
	]


	for i in paths:
		if not check_path_exists(i):
			os.mkdir(i)

	print("Made directory structure for binary files")
	print("./picklefiles")

	for i in paths:
		print(i)


set_up_picklefiles_dir()

##############################################################################
# OBJECTS
##############################################################################
print("Loading objects ...\n")

dataDir = "../data/coco/"

ld_ob = LoadData(dataDir)    # (dataDir='../data/coco/')
# (dataDir='../data/coco/', dataType=['train2017', 'val2017'])
co_ob = CocoHelper(dataDir)

ld_bin_ob = LoadBin()
sv_ob = SaveObjects()


coco1, coco2 = co_ob.coco1, co_ob.coco2  # train, val objects of coco
cc1 = co_ob.coco1['loaded_annFile']
cc2 = co_ob.coco2['loaded_annFile']  # train, val loaded annptations


if not check_path_exists("./picklefiles/cc_train"):
	joblib.dump(cc1, "./picklefiles/cc_train", compress=3)
	print("cc_train dumped at ./picklefiles/cc_train")

if not check_path_exists("./picklefiles/cc_val"):
	joblib.dump(cc2, "./picklefiles/cc_val", compress=3)
	print("cc_val dumped at ./picklefiles/cc_val")


##############################################################################
# CATEGORIES & DICT
##############################################################################
print("Loading categories and DICT ...\n")

sup_c, c = ld_ob.get_all_category_dict(
	cc1, coco1['categories'], coco1['sup_categories'])
# print("\nCategories:\n",c)


if not check_path_exists("./picklefiles/DICT"):
	joblib.dump(c, "./picklefiles/DICT")

	print("DICT dumped at ./picklefiles/DICT")

print("\nDICT\n", c)


##############################################################################
# REVERSE DICT
##############################################################################
print("Loading REVERSE_DICT ...\n")

vals = list(c.keys())
keys = list(c.values())

reverse_dict = {}
for i, j in zip(keys, vals):
	for k in i:
		reverse_dict[k] = j

if not check_path_exists("./picklefiles/REVERSE_DICT"):
	joblib.dump(reverse_dict, "./picklefiles/REVERSE_DICT")

	print("REVERSE_DICT dumped at ./picklefiles/REVERSE_DICT")

print("\nREVERSE_DICT\n", reverse_dict)


##############################################################################
# IMAGE PATH DATAFRAME
##############################################################################
print("Loading img_path_df ...\n")

img_path_df = ld_ob.getImgPath_DataFrame()  # get dataframe of all images

if not check_path_exists("./picklefiles/img_path_df"):
	joblib.dump(img_path_df, "./picklefiles/img_path_df")
	print("\nIMG_PATH_DF dumped at ./picklefiles/img_path_df")


##############################################################################
# MASTER DATAFRAMES
##############################################################################
print("Loading master dataframes ...\n")

master_df_train = co_ob.getImgCat_Datframe(cc1, "train")
master_df_val = co_ob.getImgCat_Datframe(cc2, "val")


if not check_path_exists("./picklefiles/train_master_df"):
	joblib.dump(train_master_df, compress=9)
	print("master_df_train dumped at ./picklefiles/train_master_df")

if not check_path_exists("picklefiles/val_master_df"):
	joblib.dump(master_df_val, "picklefiles/val_master_df", compress=9)
	print("master_df_val dumped at ./picklefiles/val_master_df")


##############################################################################
#  Making BINARIES of category dataframes
##############################################################################
print("Creating Binary category dataframes ...\n")

for i in range(2):
	if i == 0:
		type_of_df = "train"
		coco = cc1
		master_df = master_df_train
	if i == 1:
		type_of_df = "val"
		coco = cc2
		master_df = master_df_val

	sv_ob.save_df_all_category(
		coco, co_ob, c, img_path_df, master_df, type_of_df)


# print("Created Binary category dataframes")


print("Saving masks of all categories with color codes ...")
for k in tqdm(list(reverse_dict.keys())):
	for i in range(2):
		if i == 0:
			type_mask = "train"
			coco = cc1
			master_df = master_df_train
		if i == 1:
			type_mask = "val"
			coco = cc2
			master_df = master_df_val

		sv_ob.make_mask_objs(coco, co_ob, master_df, img_path_df,
							 cat_id=k, type_mask=type_mask, save_as="bin", mask_channels=1)

# print("Saved masks of all categories with color codes")


end_time = time.time()
total_time = end_time - start_time
print("Completed saving binaries in {}".format(total_time))


ans = input("Do you want to run a check (y/n)")
if ans == 'y' or ans == "Y":
	temp_df = ld_bin_ob.fetch_df_bin(1, "train")

	img_id = 531
	cat_id = 1

	path_to_img = temp_df[temp_df['id'] == img_id]['path'].values[0]

	reqd_mask = ld_bin_ob.fetch_mask_bin(cat_id, img_id, "train")

	co_ob.plot_img_and_mask(img_id, reqd_mask, img_path_df)

	print("Verified")

print("*****************************************************************************\n")
print("********************************  END  **************************************\n")
print("*****************************************************************************\n")
