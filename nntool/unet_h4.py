import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, MaxPool2D, MaxPooling2D, UpSampling2D, Conv2DTranspose
from tensorflow.keras.layers import Dropout, BatchNormalization, ZeroPadding2D
from tensorflow.keras.layers import Input, ReLU, ActivityRegularization
from tensorflow.keras.layers import SpatialDropout2D
from tensorflow.keras.activations import sigmoid,softmax
from tensorflow.keras.layers import concatenate
from tensorflow.keras import backend as K
from tensorflow.keras import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import *
from tensorflow.keras.regularizers import l1,l2

import numpy as np
import os

# tf.config.experimental_run_functions_eagerly(True) 


smooth = 1.

def get_min_max(x):
    return np.min(x.numpy()), np.max(x.numpy())


def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)


    y_true_f = tf.clip_by_value(y_true_f,0,1)
    y_pred_f = tf.clip_by_value(y_pred_f,0,1)
    """
    print("\n**************************************************")
    print("Y_TRUE MIN MAX {}".format(get_min_max(y_true)))
    print("Y_PRED MIN MAX {}".format(get_min_max(y_pred)))
    """

    
    # print(min_val, max_val)

    intersection = K.sum(y_true_f * y_pred_f)

    dc = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

    """
    print("NUM {}".format((2. * intersection + smooth).numpy()))
    print("DEN {}".format((K.sum(y_true_f) + K.sum(y_pred_f) + smooth).numpy()))
    print("DICE {}".format(dc.numpy()))
    print("\n**************************************************")
    """

    return dc



def dice_loss(y_true, y_pred):

    return 1.-dice_coef(y_true, y_pred)




















def conv_block(
    inputs,
    filters,
    conv_kernel,
    pool_kernel,
    pool_strides,
    block_no,
    is_bottleneck=False,
    dropout_rate=0.5,
    activation='relu',
    padding='same',
    dropout=False,
    l1_reg=None,
    l2_reg=None,
):

    names = {
        "conv1": "block_"+str(block_no)+"_conv1",
        "conv2": "block_"+str(block_no)+"_conv2",

        "dropout1": "block_"+str(block_no)+"_dropout1",
        "dropout2": "block_"+str(block_no)+"_dropout2",
        

        "batchnorm1": "block_"+str(block_no)+"_batchnorm1",
        "batchnorm2": "block_"+str(block_no)+"_batchnorm2",

        "activation1": "block_"+str(block_no)+"_relu1",
        "activation2": "block_"+str(block_no)+"_relu2",

        "maxpool": "block_"+str(block_no)+"_maxpool"
    }

    b1 = Conv2D(
        filters=filters,
        kernel_size=conv_kernel,
        padding=padding,
        name=names['conv1']
    )(inputs)
    b2 = ReLU(name=names['activation1'])(b1)

    b3 = BatchNormalization(name=names['batchnorm1'])(b2)
    
    if dropout:
        b3 = SpatialDropout2D(dropout_rate, data_format='channels_last', name=names['dropout1'])(b3)
    
    b4 = Conv2D(
        filters=filters,
        kernel_size=conv_kernel,
        padding=padding,
        name=names['conv2']
    )(b3)
    b5 = ReLU(name=names['activation2'])(b4)

    b6 = BatchNormalization(name=names['batchnorm2'])(b5)
    
    
    if dropout and is_bottleneck:
        b6 = SpatialDropout2D(dropout_rate, data_format='channels_last', name=names['dropout2'])(b6)
    
    
    b7 = MaxPool2D(
        pool_size=pool_kernel,
        strides=pool_strides,
        padding=padding,
        name=names['maxpool'])(b6)
    
    if dropout and not is_bottleneck:
        b7 = SpatialDropout2D(dropout_rate, data_format='channels_last', name=names['dropout2'])(b7)

    if not is_bottleneck:
        return b7,b6
    if is_bottleneck:
        return b6


# t = conv_block(inputs, filters[0], conv_kernel, pool_kernel, pool_strides, 1)

# Expansion Block

def upconv_block(
    inputs,
    to_concat,
    filters,
    conv_kernel,
    pool_kernel,
    pool_strides,
    block_no,
    use_transconv2d=False,
    use_upsample2d=True,
    dropout_rate=0.2,
    activation='relu',
    padding='same',
    dropout=False,
    l1_reg=None,
    l2_reg=None,
):

    names = {
        "concat": "block_"+str(block_no)+"_concat",
        "transconv": "block_"+str(block_no)+"_transconv",
        "upsample": "block_"+str(block_no)+"_upsample",

        "conv1": "block_"+str(block_no)+"_conv1",
        "conv2": "block_"+str(block_no)+"_conv2",

        "dropout1": "block_"+str(block_no)+"_dropout1",
        "dropout2": "block_"+str(block_no)+"_dropout2",
        

        "batchnorm1": "block_"+str(block_no)+"_batchnorm1",
        "batchnorm2": "block_"+str(block_no)+"_batchnorm2",

        "activation1": "block_"+str(block_no)+"_relu1",
        "activation2": "block_"+str(block_no)+"_relu2",

        "maxpool": "block_"+str(block_no)+"_maxpool"
    }

    if use_upsample2d == True and use_transconv2d == False:
        b1 = UpSampling2D(
            size=(2, 2),
            name=names['upsample'],
            interpolation='bilinear'
        )(inputs)
    if use_transconv2d == True and use_upsample2d == False:
        b1 = Conv2DTranspose(
            filters=filters,
            kernel_size=conv_kernel,
            padding=padding,
            name=names['transconv']
        )(inputs)
    
    # print(b1.shape)
    b2 = concatenate([to_concat, b1], name=names['concat'])

    b3 = Conv2D(
        filters=filters,
        kernel_size=conv_kernel,
        padding=padding,
        name=names['conv1']
    )(b2)
    b4 = ReLU(name=names['activation1'])(b3)

    b5 = BatchNormalization(name=names['batchnorm1'])(b4)
    
    if dropout:
        b5 = SpatialDropout2D(dropout_rate, data_format='channels_last', name=names['dropout2'])(b5)
        
        
    b6 = Conv2D(
        filters=filters,
        kernel_size=conv_kernel,
        padding=padding,
        name=names['conv2']
    )(b5)
    b7 = ReLU(name=names['activation2'])(b6)

    b8 = BatchNormalization(name=names['batchnorm2'])(b7)

    if dropout:
        b8 = SpatialDropout2D(dropout_rate, data_format='channels_last', name=names['dropout2'])(b8)

    return b8

"""
t = upconv_block(block_4_op, to_concat=block_4_conv2, filters=filters[2],
                 conv_kernel=conv_kernel, pool_kernel=pool_kernel,
                 pool_strides=pool_strides, block_no=5,
                 use_upsample2d=True, use_transconv2d=False)
t"""
# None

"""
Create Unet of height 3
"""

dropout_rate = 0.5
input_shape = (256, 256, 1)
s1,s2,s3 = input_shape 

inputs = Input(shape=input_shape, name='input')

filters = [64,128,256,512]
conv_kernel = (3,3)
pool_kernel = (2,2)
pool_strides = (2,2)
padding = 'same'


inputs = Input(shape=input_shape, name='input')


# conv->relu->dropout->batch_norm->conv

##################################################################
###########################  BLOCK 1 #############################
##################################################################

block_1_op, block_1_conv2 = conv_block(inputs, filters[0], conv_kernel, pool_kernel, pool_strides, 1)

##################################################################
###########################  BLOCK 2 #############################
##################################################################

block_2_op, block_2_conv2 = conv_block(block_1_op, filters[1], conv_kernel, pool_kernel, pool_strides, 2)

##################################################################
###########################  BLOCK 3 #############################
##################################################################

block_3_op, block_3_conv2 = conv_block(block_2_op, filters[2], conv_kernel, pool_kernel, pool_strides, 3)

##################################################################
###########################  BLOCK 4 #############################
##################################################################
"""
Bottleneck
"""

block_4_op = conv_block(block_3_op, filters[2],
                        conv_kernel, pool_kernel, pool_strides, 4,
                        is_bottleneck=True,dropout=True)

"""
Bottleneck
"""

##################################################################
###########################  BLOCK 5 #############################
##################################################################

block_5_op = upconv_block(block_4_op, to_concat=block_3_conv2, filters=filters[2],
                 conv_kernel=conv_kernel, pool_kernel=pool_kernel,
                 pool_strides=pool_strides, block_no=5,
                 use_upsample2d=True, use_transconv2d=False)

##################################################################
###########################  BLOCK 6 #############################
##################################################################

block_6_op = upconv_block(block_5_op, to_concat=block_2_conv2, filters=filters[1],
                 conv_kernel=conv_kernel, pool_kernel=pool_kernel,
                 pool_strides=pool_strides, block_no=6,
                 use_upsample2d=True, use_transconv2d=False)

##################################################################
###########################  BLOCK 7 #############################
##################################################################

block_7_op = upconv_block(block_6_op, to_concat=block_1_conv2, filters=filters[0],
                 conv_kernel=conv_kernel, pool_kernel=pool_kernel,
                 pool_strides=pool_strides, block_no=7,
                 use_upsample2d=True, use_transconv2d=False)



final_conv = Conv2D(
    filters=1,
    kernel_size=conv_kernel,
    padding=padding,
    name="final_conv"
)(block_7_op)

final_activ = sigmoid(final_conv)

final_op = final_activ

outputs = final_op


model = Model(inputs=[inputs], outputs=[outputs])
# model.summary(positions=[.33, .60, .70, 1.])