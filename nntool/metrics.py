import numpy as np



class Metrics:
	"""Class for Metrics and Losses"""

	def __init__(self):
		pass


	def get_elements_confusion_matrix(self,mask_true,mask_pred):
		
		TP = np.bitwise_and(mask_true, mask_pred)
		TP[TP==255] = 0

		FN = np.bitwise_not(mask_true) - np.bitwise_not(mask_pred)
		FN[FN==255] = 0

		TN = np.bitwise_not(np.bitwise_and(mask_true, mask_pred))
		TN[TN==255] = 0
		TN = np.bitwise_not(TN)

		FP = mask_true - mask_pred 
		FP[FP==255] = 0
		
		return (TP,FN,TN,FP)



	def jaccard_index(self,mask_true,mask_pred):


		TP,FN,TN,FP = self.get_elements_confusion_matrix(mask_pred,mask_true)

		Ji = np.sum(TP)/(np.sum(TP)+np.sum(FP)+np.sum(FN))

		return Ji

	def dice_coefficient(self,mask_true,mask_pred):

		TP,FN,TN,FP = self.get_elements_confusion_matrix(mask_pred,mask_true)
		D = 2*np.sum(TP)/(2*np.sum(TP)+np.sum(FP)+np.sum(FN))

		return D