import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, MaxPool2D, MaxPooling2D, UpSampling2D, Conv2DTranspose
from tensorflow.keras.layers import Dropout, BatchNormalization
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import concatenate
from tensorflow.keras import backend as K
from tensorflow.keras import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import *

import numpy as np
import os

smooth = 1.


def dice_coef(y_true, y_pred):

	y_true_f = K.flatten(y_true)
	y_pred_f = K.flatten(y_pred)


	y_true_f = tf.clip_by_value(y_true_f,0,1)
	y_pred_f = tf.clip_by_value(y_pred_f,0,1)

	"""
	m1 = tf.math.reduce_min(tf.clip_by_value(y_true_f,0,1))
	m2 = tf.math.reduce_min(tf.clip_by_value(y_pred_f,0,1))

	m1_ = tf.math.reduce_max(tf.clip_by_value(y_true_f,0,1))
	m2_ = tf.math.reduce_max(tf.clip_by_value(y_pred_f,0,1))
	
	
	print()
	print("*"*10)
	print("yTRUE min max______\n",m1,m1_)
	print()
	print("yPRED min max______\n",m2,m2_)
	print()
	"""

	# print(y_true_f.shape, y_pred_f.shape)
	
	intersection = K.sum(y_true_f * y_pred_f)

	dc = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
	# print("dc______\n",dc)

	return dc



def dice_coef_loss(y_true, y_pred):
	
	
	
	"""
	m1 = tf.math.reduce_min(tf.clip_by_value(y_true,0,1))
	m2 = tf.math.reduce_min(tf.clip_by_value(y_pred,0,1))

	m1_ = tf.math.reduce_max(tf.clip_by_value(y_true,0,1))
	m2_ = tf.math.reduce_max(tf.clip_by_value(y_pred,0,1))
	
	
	print()
	print("*"*10)
	print("yTRUE min max______\n",m1,m1_)
	print()
	print("yPRED min max______\n",m2,m2_)
	print()
	"""	

	return 1-dice_coef(y_true, y_pred)
	

def iou(y_true, y_pred, smooth=1.):
	intersection = K.sum(K.abs(y_true * y_pred), axis=-1)
	union = K.sum(y_true,-1) + K.sum(y_pred,-1) - intersection
	iou = (intersection + smooth) / ( union + smooth)
	return iou
	
	


def conv_block(
	inputs,

	filters=16,
	kernel_size=(3, 3),
	activation='relu',
	padding='same',
	dropout_rate=0.2,
	dropout=True,
	use_batch_norm=True,

):

	c = Conv2D(filters, kernel_size=kernel_size,
						 activation=activation, padding=padding)(inputs)

	if use_batch_norm:
			c = BatchNormalization()(c)

	if dropout > 0.0:
			c = Dropout(dropout_rate)(c)

	c = Conv2D(filters, kernel_size=kernel_size,
						 activation=activation, padding=padding)(c)

	if use_batch_norm:
			c = BatchNormalization()(c)

	return c


def transconv_block(
	inputs,

	filters=16,
	kernel_size=(2, 2),
	strides=(2, 2),
	activation='relu',
	padding='same',
):
	ct = Conv2DTranspose(filters, kernel_size, strides=strides,
											 padding=padding, activation=activation)(inputs)

	return ct


def custom_unet(
	depth=4,
	num_classes=1,
	filters=16,
	input_shape=(512, 512, 3),
	activation='relu',
	dropout_rate=0.2,
	learning_rate=1e-4,
	dropout=True,
	use_batch_norm=True,
	verbose=True,):
	"""

	"""

	inputs = Input(shape=input_shape)
	x = BatchNormalization()(inputs)

	conv_layers = []

	####################
	#### Downsample ####
	####################


	for i in range(depth):
			x = conv_block(inputs=x, filters=filters, use_batch_norm=use_batch_norm,
										 dropout=dropout, dropout_rate=dropout_rate)
			conv_layers.append(x)

			x = MaxPooling2D((2,2))(x)

			filters = filters*2


	####################
	#### Bottleneck ####
	####################

	x = conv_block(inputs=x, filters=512, use_batch_norm=use_batch_norm,
										 dropout=dropout, dropout_rate=dropout_rate)


	####################
	##### Upsample #####
	####################

	for i in reversed(conv_layers):
			filters //= 2

			x = transconv_block(x, filters, (2, 2), strides=(2, 2), padding='same')
			x = concatenate([x,i])
			# print(x)
			x = conv_block(inputs=x, filters=filters, use_batch_norm=use_batch_norm,
										 dropout=dropout, dropout_rate=dropout_rate)


	op = Conv2D(num_classes,(1,1),activation='sigmoid')(x)


	model = Model(inputs=[inputs], outputs=[op])
	
	if verbose:
		print(model.summary())

	return model


# inputs = Input(shape=(256, 256, 3))
# model = custom_unet(depth=3,verbose=True)


# model.compile(optimizer=Adam(lr=1e-4), loss=dice_coef_loss, metrics=[dice, iou])
